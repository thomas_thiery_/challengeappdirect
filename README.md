# appdirect-code-challenge-solution

app direct challenge

## Requirement

node v8.1.2
npm 5.0.3

## Setup

Install the project dependencies:

`npm install`

## Running

Starts the static and proxy servers:

`npm start`

