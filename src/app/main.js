import React, {Component} from 'react';
import {Title} from './title';
import {Tweets} from './tweets/tweets';
import {Footer} from './footer';

export class Main extends Component {
  render() {
    return (
      <div>
        <main>
          <Title/>
          <Tweets/>
        </main>
        <Footer/>
      </div>
    );
  }
}
