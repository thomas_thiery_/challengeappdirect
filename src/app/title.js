import React, {Component} from 'react';

const styles = {
  h1: {
    fontWeight: 300,
    fontSize: '3rem',
    lineHeight: '6rem',
    padding: '0 1rem',
    margin: '0',
    backgroundColor: '#1f2326',
    color: 'white'
  }
};

export class Title extends Component {
  render() {
    return (
      <div>
        <h1 style={styles.h1}>AppDirect Challenge</h1>
      </div>
    );
  }
}
