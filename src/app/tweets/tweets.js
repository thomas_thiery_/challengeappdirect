import React, {Component} from 'react';
import axios from 'axios';

import {Tweet} from './tweet';

const accountTwitterNames = ['AppDirect', 'LaughingSquid', 'TechCrunch'];
const limit = 30;
const allRequest = [];

const styles = {
  container: {
    margin: '1rem'
  },
  h2: {
    fontWeight: 300,
    fontSize: '1.5rem'
  },
  tweet: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  column: {
    flexGrow: 1
  }
};

export class Tweets extends Component {
  constructor() {
    super();
    this.state = {tweetsInAccount: []};
  }

  componentDidMount() {
    accountTwitterNames.forEach(account => {
      allRequest.push(
        axios.get(`http://localhost:7890/1.1/statuses/user_timeline.json?user_id=${account}&screen_name=${account}&count=${limit}`)
      );
    });

    Promise.all(allRequest).then(response => {
      this.setState({tweetsInAccount: response});
    });
  }

  render() {
    return (
      <div style={styles.container}>
        <h2 style={styles.h2}>
          Last 30 tweets from : {accountTwitterNames.map(account => (
          `${account} `
        ))}
        </h2>
        <div style={styles.tweet}>
          {this.state.tweetsInAccount.map((tweets, j) => (
            <div key={j} style={styles.column}>
              { tweets.data.map((tweet, i) => (
                <Tweet key={i} tweet={tweet}/>
              ))}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
