import React, {Component} from 'react';
import Moment from 'react-moment';

const styles = {
  tweet: {
    width: '15rem',
    border: '1px solid lightgray',
    borderRadius: '1rem',
    margin: '1rem',
    padding: '1rem',
    backgroundColor: '#1DA1F2'
  }
};

export class Tweet extends Component {
  render() {
    return (
      <div style={styles.tweet}>
        <p>{this.props.tweet.text}</p>
        <p><Moment format="DD/MM/YYYY">{this.props.tweet.created_at}</Moment></p>
        <p>
          <a
            href={`https://www.twitter.com/${this.props.tweet.user.screen_name}/status/${this.props.tweet.id_str}`}
            target="_blank"
            rel="noopener noreferrer"
            >
            Tweet link
          </a>
        </p>
        <p>
          {this.props.tweet.quoted_status ? (this.props.tweet.quoted_status.user.screen_name) : (' ')}
          {this.props.tweet.retweeted_status ? (this.props.tweet.retweeted_status.user.screen_name) : (' ')}
        </p>
        <p>
          {this.props.tweet.entities.user_mentions.length ? ('Mention: ' + this.props.tweet.entities.user_mentions.map(user => (
            user.screen_name + ' '
          ))
        ) : (' ')}
        </p>
      </div>
    );
  }
}

Tweet.propTypes = {
  tweet: React.PropTypes.object.isRequired
};
